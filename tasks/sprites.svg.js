'use strict'

const gulp = require('gulp');
const del  = require('del');
const $    = require('gulp-load-plugins')();

module.exports = function(options) {
	return function(){
		return gulp.src("*.svg", {cwd: 'src/assets/sprites-svg/'})
			.pipe($.svgSprite({
				mode: {
					css: {
						sprite: '../img/sprite.svg',
						dimensions : '-size',
                		prefix : '.icon-%s',
						dest: '.',
						render: {
							scss: true
						}
					}
				},
			}))
			.pipe(gulp.dest(function(file){
				return file.extname != '.scss' ? `./dist/assets/assets` : './src/assets/sass/base'
			}))
			
	};
};


    