'use strict'

const gulp = require('gulp');
const $    = require('gulp-load-plugins')();

module.exports = function(options) {
	return function(){
		return gulp.src(options.src)
	        .pipe($.cached(options.taskName))
	        .pipe($.responsiveImages({
	        	'**/*.{png,jpg,gif}': [
	                {
	                    crop: false,
	                    withMetadata: true,
	                    quality: 80,
	                    suffix: '@2x'
	                },
	                {
	                    crop: false,
	                    withMetadata: true,
	                    quality: 80,
	                    width: "50%"
	                }
	            ]
	        }))
	        .pipe(gulp.dest(options.dest));
	};
};
