'use strict'

const gulp = require('gulp');
const $    = require('gulp-load-plugins')();


module.exports = function(options) {
	return function(){
		return gulp.src(options.src)
			.pipe($.sourcemaps.init())
			.pipe($.sass().on('error', $.sass.logError))
	        .pipe($.cached(options.taskName))
	        .pipe($.autoprefixer({
	        	browsers: ['last 10 versions'],
            	cascade: false
	        }))
	        .pipe($.groupCssMediaQueries())
	        .pipe($.uglifycss())
	        .pipe($.rename({
	        	suffix: ".min"
	        }))
	        .pipe($.sourcemaps.write())
	        .pipe(gulp.dest(options.dest))
	        
	};
};
