'use strict'

const gulp      = require('gulp');
const smartgrid = require('smart-grid');

module.exports = function(options) {
	return function(callback){
		return {
			a: smartgrid(options.dest, options.settings),	
			b: callback(),
		} 
				
	};
};