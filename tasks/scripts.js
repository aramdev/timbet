'use strict'

const gulp  = require('gulp');
const $     = require('gulp-load-plugins')();


module.exports = function(options) {
	return function(){
		return gulp.src(options.src)
			.pipe($.sourcemaps.init())
	        .pipe($.include())
			.pipe($.babel({
				presets: ['env']
			}))
			.pipe($.jsMinify())
			.pipe($.rename({
	        	suffix: ".min"
	        }))
	        .pipe($.sourcemaps.write())
			.pipe(gulp.dest(options.dest));
	};
};