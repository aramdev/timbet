var moduleCards = {
    cacheDom() {
        this.$owl = $('.js-cards')
        this.$name = $('.js-cards .name')
        
    },
    runCorusel: function(){
        this.$owl.owlCarousel({
            loop:true,
            nav:true,
            dots: true,
            navText: ['<i class="icn-treg-left"></i>', '<i class="icn-treg-right"></i>'],
            responsive:{
                0:{
                    items: 3
                },
                991:{
                    items: 4
                },
                1199:{
                    items: 5
                }

            }
        });
    },
    events () {
        this.$owl.on('initialize.owl.carousel', () => {
            this.$name.each(function(index, el) {
                let text = $(el).text().split("")
                let html = ""
                for(let i = 0; i < text.length; i++){
                    html = html + `<span>${text[i]}</span>`    
                }
                $(el).html(html)
            }); 
        })
    },
    
    init () {
        this.cacheDom()
        this.events()
        this.runCorusel()
    }
}

if($('.js-cards').length > 0){
    moduleCards.init()
}