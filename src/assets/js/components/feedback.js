var moduleFeedback = {
    cacheDom() {
        this.$cnt = $('.js-feedback')
        this.$prev = this.$cnt.find('.js-prev');
        this.$next = this.$cnt.find('.js-next');
        this.$carousel = this.$cnt.find('.js-carousel');
        
    },
    prevSlide () {
        let flag =  this.$carousel.find('[data-num="1"]').prev().index()
        if(flag >= 0){
            this.$carousel.find('[data-num]').each(function(index, el) {
                let num = parseInt($(el).attr('data-num'))
                if (num == 7){
                    $(el).removeAttr('data-num')
                }else{
                    if(num == 1){
                        $(el).prev().attr('data-num', '1') 
                    }
                    $(el).attr('data-num', num + 1)
                }
            });
            this.$next.show();
        }else{
            this.$prev.hide();
        }
    }, 
    nextSlide () {
        let flag =  this.$carousel.find('[data-num="7"]').next().index()
        if(flag >= 0){
            this.$carousel.find('[data-num]').each(function(index, el) {
                let num = $(el).attr('data-num')
                if (num == 1){
                    $(el).removeAttr('data-num')
                }else{
                    if(num == 7){
                        $(el).next().attr('data-num', '7') 
                    }
                    $(el).attr('data-num', num - 1)
                }
            });
            this.$prev.show();
        }else{
            this.$next.hide();
        }
    }, 
    events () {
        this.$prev.on('click', this.prevSlide.bind(this));
        this.$next.on('click', this.nextSlide.bind(this));
    },
    init () {
        this.cacheDom()
        this.events()
    }
}

if($('.js-feedback').length > 0){
    moduleFeedback.init()
}


