var moduleFreeForecasts = {
    cacheDom() {
        this.$table = $('.js-freeForecasts')
    },
    tableInit () {
        this.$table.footable({
            "breakpoints": {
                "xs": 480,
                "sm": 768,
                "md": 992,
                "lg": 1200,
                "xl": 1400
            },
            "showToggle": false
        })
    },
    init () {
        this.cacheDom()
        this.tableInit()
    }
}

if($('.js-freeForecasts').length > 0){
    moduleFreeForecasts.init()
}