var moduleScrollTo = {
    cacheDom() {
        this.$btn = $('[data-scrollTo]')
        this.$html = $('html, body')
        this.$win = $(window)
    },
    scrollTo (e) {
        let div = $(e.currentTarget).attr('data-scrollTo');
        this.$html.animate({scrollTop: $(div).offset().top}, 500);
    },
    events () {
        this.$btn.on('click', this.scrollTo.bind(this));
    },
    init () {
        this.cacheDom()
        this.events()
    }
}

if($('[data-scrollTo]').length > 0){
    moduleScrollTo.init()
}